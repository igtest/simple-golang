package main
import (
"testing"
)
func TestGetSumBasic(t *testing.T) {
ans := GetSum("2", "2")
if ans != "2+2=4" {
t.Errorf("getSum(2, 2) = %s; want 4", ans)
}
}
func TestGetSubBasic(t *testing.T) {
ans := GetSubtraction("3", "2")
if ans != "3-2=1" {
t.Errorf("GetSubtraction(3, 2) = %s; want 1", ans)
}
}