package main
import (
"fmt"
"io"
"log"
"os"
"strconv"
"github.com/gin-gonic/gin"
)
func main() {
	myfile, _ := os.Create("server.log")
	gin.DefaultWriter = io.MultiWriter(myfile, os.Stdout)
	gin.SetMode(gin.ReleaseMode)
	
	r := gin.Default()
    v1 := r.Group("api/v1")
    {v1.GET("/calc", calculate)}
	
	r.GET("/health", index)
	r.Run(":8080")
}

func calculate(c *gin.Context) { //http://localhost:8081/api/v1/calc?num1=5&operator=sum&num2=3
	num1 := c.Query("num1")
	operator := c.Query("operator")
	num2 := c.Query("num2")
	job := struct {
	  Num1 string `json:"num1"`
	  Operator string `json:"operator"`
	  Num2 string `json:"num2"`
	}{num1, operator, num2}
	
	fmt.Printf("Operator: %s \n", job.Operator)
	fmt.Printf("Num1: %s, Operator: %s, Num2: %s \n", job.Num1, job.Operator, job.Num2)
	
	if operator == "sum" {
	result := GetSum(num1, num2)
	fmt.Println(operator, "is sumjob", result)
		c.JSON(200, gin.H{
			"Calculate": result,
		})
	} else if operator == "sub" {
	result := GetSubtraction(num1, num2)
	fmt.Println(operator, "is subsjob", result)
		c.JSON(200, gin.H{
			"Calculate": result,
		})
	} else {
		c.JSON(200, gin.H{
			"What is this? you do somethith wrong? man ": operator,
		})
	}
}
	// Функций для выполнения операций
func GetSum(a, b string) string {
	num1, err := strconv.Atoi(a)
	if err != nil {log.Fatal("Error converting number to int: ", err)}
	num2, err := strconv.Atoi(b)
	if err != nil {log.Fatal("Error converting number to int: ", err)}
	
	return fmt.Sprintf("%d+%d=%d", num1, num2, num1+num2)
}

func GetSubtraction(a, b string) string {
	num1, err := strconv.Atoi(a)
	if err != nil {log.Fatal("Error converting number to int: ", err)}
	num2, err := strconv.Atoi(b)
	if err != nil {log.Fatal("Error converting number to int: ", err)}
	
	return fmt.Sprintf("%d-%d=%d", num1, num2, num1-num2)
}

func index(c *gin.Context) {
	content := gin.H{"HOLA INNOPOLIS": "SOME CRUTCH API work!"}
	c.JSON(200, content)
}